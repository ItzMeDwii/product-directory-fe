/** @type {import('tailwindcss').Config} */
module.exports = {
	content: [
		"./src/layouts/**/*.{js,ts,jsx,tsx}",
		"./src/pages/**/*.{js,ts,jsx,tsx}",
		"./src/components/**/*.{js,ts,jsx,tsx}",
	],
	theme: {
		extend: {
			fontFamily: {
				rubik: ["Rubik", "sans-serif"],
			},
			container: {
				padding: {
					DEFAULT: "1rem",
					sm: "1rem",
					md: "1rem",
					lg: "0rem",
					xl: "0rem",
					"2xl": "0rem",
				},
			},
			colors: {
				primary: {
					100: "#cdeecf",
					200: "#9ade9f",
					300: "#68cd6e",
					400: "#35bd3e",
					500: "#03ac0e",
					600: "#028a0b",
					700: "#026708",
					800: "#014506",
					900: "#012203"
				},
				primary2: {
					100: "#cdebcf",
					200: "#9bd79f",
					300: "#68c46e",
					400: "#36b03e",
					500: "#049c0e",
					600: "#037d0b",
					700: "#025e08",
					800: "#023e06",
					900: "#011f03"
				},
			}
		},
	},
	plugins: [
		require("@thoughtbot/tailwindcss-aria-attributes"),
		require('@headlessui/tailwindcss')
	],
};
