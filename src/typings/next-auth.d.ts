// eslint-disable-next-line @typescript-eslint/no-unused-vars
import NextAuth, { DefaultSession } from "next-auth";

declare module "next-auth" {
	interface User {
		name?: string;
		id?: number;
		username?: string;
		email?: string;
		firstName?: string;
		lastName?: string;
		gender?: string;
		image?: string;
	}

	/**
	 * Returned by `useSession`, `getSession` and received as a prop on the `SessionProvider` React Context
	 */
	interface Session {
		user: User;
	}
}
