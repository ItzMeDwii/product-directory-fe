export type ICategory = string;

export interface IProduct {
	id: number;
	title: string;
	description?: string;
	quantity?: number;
	total?: number;
	price: number;
	discountPercentage: number;
	rating?: number;
	stock?: number;
	brand?: string;
	category?: ICategory;
	thumbnail?: string;
	images?: string[];
}

export type ICategories = ICategory[];

export interface IProducts {
	products: IProduct[];
	total: number;
	skip: number;
	limit: number;
}

declare type ProductsQueryFilter = "category" | "single";
declare type ProductsQueryId = string[] | number | string;
