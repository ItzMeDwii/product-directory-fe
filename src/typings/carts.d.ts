import { Product } from "./products";

export interface ICart {
	id: number;
	products: Product[];
	total: number;
	discountedTotal: number;
	userId: number;
	totalProducts: number;
	totalQuantity: number;
	createdAt?: number;
}

export interface ICarts {
	carts: Cart[];
	total: number;
	skip: number;
	limit: number;
}

declare type CartsQueryId = string[] | number | string;
