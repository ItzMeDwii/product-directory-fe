import BottomNavigation from "@components/BottomNavigation";
import ItemNotFound from "@components/utils/ItemNotFound";
import MainLayout from "@layouts/main";

const Custom404 = () => {
	return (
		<>
			<div className="pt-6 px-4">
				<ItemNotFound />
			</div>
			<BottomNavigation />
		</>
	);
};

Custom404.layout = MainLayout;

export default Custom404;
