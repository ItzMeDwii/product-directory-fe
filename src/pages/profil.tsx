import Loader from "@components/utils/Loader";
import MainLayout from "@layouts/main";
import textCapitalize from "@utils/textCapitalize";
import { signIn, signOut, useSession } from "next-auth/react";
import BottomNavigation from "@components/BottomNavigation";
import Image from "next/image";
import { PhoneIcon } from "@heroicons/react/24/outline";

const Profile = () => {
	const { data: session, status } = useSession();

	return (
		<>
			<section className="h-[346px] w-full bg-gradient-to-r from-[#01da10] to-[#017109] rounded-b-2xl overflow-hidden relative z-[2]">
				<span className="absolute w-[120px] h-[120px] bg-primary-500 top-[-38px] right-[-54px] rounded-2xl -rotate-45 z-[1]"></span>
				<span className="absolute w-[120px] h-[120px] bg-primary-600 bottom-[40px] left-[-60px] rounded-2xl -rotate-45 z-[1]"></span>

				<div className="absolute mt-4 px-4">
					<h1 className="text-xl font-bold text-white">Profil</h1>
				</div>

				<div className="flex h-[346px] justify-center items-center text-white">
					<div>
						<div className="flex justify-center w-full">
							<Image
								className="rounded-full"
								layout="fixed"
								height={95}
								width={95}
								alt="profile"
								src={
									status === "authenticated"
										? session.user.image
										: "/user-icon.png"
								}
							/>
						</div>
						{status === "authenticated" && (
							<>
								<h1 className="text-center mt-6 font-semibold">
									{session.user.name
										? session.user.name
										: `${session.user.firstName} ${session.user.lastName}`}
								</h1>
								<h2 className="flex mt-2 text-xs font-normal">
									<PhoneIcon className="w-4 h-4 mr-2" />
									{session.user.email}
								</h2>
							</>
						)}
					</div>
				</div>
			</section>

			<section className="pt-6 px-4 pb-14 mx-auto">
				<div className="text-center">
					{status === "loading" ? (
						<div className="flex justify-center">
							<Loader />
						</div>
					) : (
						<>
							<div>
								<h1>
									Login Status:{" "}
									{`${textCapitalize(status)} ${
										status === "authenticated"
											? `(${textCapitalize(session?.provider as string)})`
											: ""
									}`}
								</h1>
							</div>

							<div className="mt-4 flex justify-center w-full">
								{status === "unauthenticated" ? (
									<button
										onClick={() => signIn()}
										type="button"
										className="text-primary-500 bg-green-200 py-2 px-3 rounded-full"
									>
										Log In
									</button>
								) : (
									<button
										onClick={() => signOut()}
										type="button"
										className="text-primary-500 bg-green-200 py-2 px-3 rounded-full"
									>
										Log Out
									</button>
								)}
							</div>
						</>
					)}
				</div>
			</section>
			<BottomNavigation />
		</>
	);
};

Profile.layout = MainLayout;

export default Profile;
