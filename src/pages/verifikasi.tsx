import Navigation from "@components/Navigation";
import MainLayout from "@layouts/main";
import Image from "next/image";

const AuthRegister = () => {
	return (
		<>
			<Navigation title="Verifikasi OTP" />
			<div className="pt-16 px-4 mx-auto min-h-screen relative">
				<div className="mt-4 flex justify-center w-full">
					<Image
						layout="fixed"
						alt="login image"
						src="/ilustration-otp.png"
						height={231}
						width={226}
					/>
				</div>

				<div className="mt-6">
					<h1 className="text-center font-bold">Verifikasi Kode OTP</h1>
					<p className="mt-3 text-center text-sm">
						Masukkan nomor OTP yang telah kami kirim ke nomor telepon Anda
					</p>
				</div>

				<div className="mt-6 flex justify-center items-center w-full space-x-4">
					<input
						type="text"
						placeholder="-"
						className="w-[74px] h-[74px] bg-gray-100 p-6 text-center rounded-md"
					/>
					<input
						type="text"
						placeholder="-"
						className="w-[74px] h-[74px] bg-gray-100 p-6 text-center rounded-md"
					/>
					<input
						type="text"
						placeholder="-"
						className="w-[74px] h-[74px] bg-gray-100 p-6 text-center rounded-md"
					/>
					<input
						type="text"
						placeholder="-"
						className="w-[74px] h-[74px] bg-gray-100 p-6 text-center rounded-md"
					/>
				</div>

				<div className="absolute bottom-6 left-0 mx-auto w-full px-4">
					<button className="text-center text-sm font-semibold bg-primary-500 text-white rounded-full py-4 w-full">
						Kirim
					</button>
				</div>
			</div>
		</>
	);
};

AuthRegister.layout = MainLayout;

export default AuthRegister;
