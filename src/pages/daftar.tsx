import Input from "@components/Input";
import Navigation from "@components/Navigation";
import MainLayout from "@layouts/main";
import {
	CursorArrowRippleIcon,
	PhoneIcon,
	UserIcon,
} from "@heroicons/react/24/outline";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import * as yup from "yup";

type LoginFormFields = {
	namalengkap: string;
	nohp: string;
	koderef: string;
};

const schema = yup
	.object({
		namalengkap: yup.string().required("Nama Lengkap tidak boleh kosong!"),
		nohp: yup.string().required("Nomor HP tidak boleh kosong!"),
		koderef: yup.string(),
	})
	.required();

const AuthRegister = () => {
	const { register, formState, handleSubmit, getFieldState, getValues } =
		useForm<LoginFormFields>({
			mode: "all",
			resolver: yupResolver(schema),
			defaultValues: {
				namalengkap: "",
				nohp: "",
				koderef: "",
			},
		});

	const onSubmit = handleSubmit((data) => {
		console.log(data);
	});

	return (
		<>
			<Navigation title="Daftar Baru" />
			<div className="pt-16 px-4 mx-auto min-h-screen relative">
				<form onSubmit={onSubmit}>
					<div className="mt-4">
						<Input<LoginFormFields>
							name="namalengkap"
							type="text"
							label="Nama Lengkap"
							register={register}
							icon={UserIcon}
							fieldValue={getValues("namalengkap")}
							fieldState={getFieldState("namalengkap", formState)}
						/>
					</div>

					<div className="mt-4">
						<Input<LoginFormFields>
							name="nohp"
							type="text"
							label="Nomor HP"
							register={register}
							icon={PhoneIcon}
							fieldValue={getValues("nohp")}
							fieldState={getFieldState("nohp", formState)}
						/>
					</div>

					<div className="mt-4">
						<Input<LoginFormFields>
							name="koderef"
							type="text"
							label="Kode Refferal (Opsional)"
							register={register}
							icon={CursorArrowRippleIcon}
							fieldValue={getValues("koderef")}
							fieldState={getFieldState("koderef", formState)}
						/>
					</div>

					<div className="absolute bottom-6 left-0 mx-auto w-full px-4">
						<button
							type="submit"
							className="text-center text-sm font-semibold bg-primary-500 text-white rounded-full py-4 w-full"
						>
							Daftar
						</button>
					</div>
				</form>
			</div>
		</>
	);
};

AuthRegister.layout = MainLayout;

export default AuthRegister;
