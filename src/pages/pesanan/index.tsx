import BottomNavigation from "@components/BottomNavigation";
import { Tab } from "@headlessui/react";
import { FunnelIcon, MagnifyingGlassIcon } from "@heroicons/react/24/outline";
import MainLayout from "@layouts/main";
import { OrderSelectValue } from "@store/order";
import { useSelector } from "react-redux";
import { ICart } from "src/typings/carts";
import Image from "next/image";
import currency from "currency.js";
import dayjs from "dayjs";
import { useState } from "react";
import Loader from "@components/utils/Loader";

const Pesanan = () => {
	const [getCarts] = useState(useSelector(OrderSelectValue));
	const carts = [...getCarts].sort(
		(a: ICart, b: ICart) => b.createdAt - a.createdAt
	);

	return (
		<>
			<div className="pt-6 px-4 pb-14 mx-auto">
				<h1 className="font-bold text-xl">Daftar Pesanan</h1>

				<section className="mt-6">
					<Tab.Group>
						<Tab.List className="flex justify-between text-sm font-semibold">
							<Tab className="focus:!outline-0 px-2 pb-2 aria-selected:text-primary-500 aria-selected:border-b-2 aria-selected:border-primary-500 text-gray-500">
								Transaksi Berlangsung
							</Tab>
							<Tab className="focus:!outline-0 px-2 pb-2 aria-selected:text-primary-500 aria-selected:border-b-2 aria-selected:border-primary-500 text-gray-500">
								Riwayat Pemesanan
							</Tab>
						</Tab.List>
						<Tab.Panels className="mt-6">
							<Tab.Panel>
								<section>
									<div className="flex items-center space-x-3 mt-2">
										<div className="flex justify-center items-center relative w-full">
											<MagnifyingGlassIcon className="absolute left-3 h-4 w-4 text-gray-700" />
											<input
												type="search"
												placeholder="Cari Transaksi"
												className="bg-gray-100 text-gray-600 rounded-full pl-9 pr-3 py-3 text-sm w-full focus:!outline-0"
											/>
										</div>
										<button className="bg-gray-100 p-3 rounded-full">
											<FunnelIcon className="h-5 w-5 text-primary-500" />
										</button>
									</div>
								</section>

								<section className="mt-5">
									{carts ? (
										carts.map((cart: ICart, index: number) => {
											return (
												<div className="mb-4" key={index}>
													<div className="bg-gray-100 rounded-xl p-3">
														<div className="relative pb-3 border-b">
															<span className="flex items-center space-x-2">
																<Image
																	layout="fixed"
																	src="/logo.png"
																	alt="woishop"
																	width={27}
																	height={30}
																/>
																<h1 className="text-primary-500 font-semibold">
																	WoiShop
																</h1>
															</span>

															<span className="absolute flex items-center rounded-full bg-green-600 py-1 px-3 top-1 right-0">
																<h1 className="text-[8px] text-white">Baru</h1>
															</span>
														</div>

														<div className="mt-4">
															<h1 className="text-sm text-gray-500">
																Total pesanan {cart.totalProducts} produk
															</h1>
															<h2 className="font-semibold mt-2">
																{currency(cart.total * 14818, {
																	symbol: "Rp. ",
																	decimal: ",",
																	separator: ".",
																	precision: 0,
																}).format()}
															</h2>

															<h3 className="text-xs text-gray-500 mt-2">
																{dayjs(cart.createdAt).format(
																	"DD MMM YYYY, HH:mm"
																)}
															</h3>
														</div>
													</div>
												</div>
											);
										})
									) : (
										<div className="mt-4 flex justify-center">
											<Loader />
										</div>
									)}
								</section>
							</Tab.Panel>
							<Tab.Panel></Tab.Panel>
						</Tab.Panels>
					</Tab.Group>
				</section>
			</div>
			<BottomNavigation />
		</>
	);
};

Pesanan.layout = MainLayout;

export default Pesanan;
