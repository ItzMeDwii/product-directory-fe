import Navigation from "@components/Navigation";
import ItemNotFound from "@components/utils/ItemNotFound";
import Loader from "@components/utils/Loader";
import { fetchProduct, useProduct } from "@hooks/index";
import MainLayout from "@layouts/main";
import { addProduct } from "@store/cart";
import { ProductsQueryId } from "@typings/products";
import textCapitalize from "@utils/textCapitalize";
import { dehydrate, QueryClient } from "@tanstack/react-query";
import currency from "currency.js";
import Image from "next/image";
import { useRouter } from "next/router";
import { useDispatch } from "react-redux";
import { signIn, useSession } from "next-auth/react";

const ProductDisplay = ({ ...props }) => {
	const { status } = useSession();
	const router = useRouter();
	const {
		data: product,
		isLoading,
		isFetching,
		isError,
	} = useProduct(props.id);

	const dispatch = useDispatch();

	if (isError)
		return (
			<div className="mt-6">
				<ItemNotFound />
			</div>
		);
	if (isLoading || isFetching)
		return (
			<div className="mt-6 flex justify-center">
				<Loader />
			</div>
		);

	return (
		<div className="mt-1">
			<div className="max-h-[343px] aspect-square relative">
				<Image
					layout="fill"
					className="rounded-lg"
					alt={product.title}
					src={product.images[0]}
				/>
			</div>

			<div className="mt-4">
				<span className="px-3 py-1 rounded-full text-xs font-semibold text-gray-600 bg-gray-200">
					{textCapitalize(product.category)}
				</span>
				<h1 className="text-sm font-semibold mt-3">{product.title}</h1>
				<h1 className="text-sm font-semibold mt-3">
					{currency(product.price * 14818, {
						symbol: "Rp. ",
						decimal: ",",
						separator: ".",
						precision: 0,
					}).format()}
				</h1>

				<hr className="mt-6" />

				<h1 className="text-sm font-semibold mt-5">Deskripsi Produk</h1>
				<p className="text-xs mt-2 text-gray-600">{product.description}</p>
			</div>
			<div className="flex justify-center w-full mt-4">
				<button
					onClick={() =>
						status === "authenticated"
							? dispatch(
									addProduct({
										product,
										quantity: 1,
									})
							  ) && router.push(`/toko/kategori/${product.category}`)
							: signIn()
					}
					className="w-full rounded-full text-sm font-semibold py-3 px-5 bg-primary-500 text-white"
				>
					Tambah Pesanan
				</button>
			</div>
		</div>
	);
};

const TokoProduk = () => {
	const router = useRouter();
	const { produkId } = router.query;

	return (
		<>
			<Navigation title="Kembali" />

			<div className="pt-16 pb-6 px-4 mx-auto">
				<ProductDisplay id={produkId} />
			</div>
		</>
	);
};

export async function getServerProps(id: ProductsQueryId) {
	const queryClient = new QueryClient();

	await queryClient.prefetchQuery(["product", id], () => fetchProduct(id));

	return {
		props: {
			dehydratedState: dehydrate(queryClient),
		},
	};
}

TokoProduk.layout = MainLayout;

export default TokoProduk;
