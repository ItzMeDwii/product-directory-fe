import Navigation from "@components/Navigation";
import ItemNotFound from "@components/utils/ItemNotFound";
import Loader from "@components/utils/Loader";
import { fetchProductsByCategory, useProductsByCategory } from "@hooks/store";
import MainLayout from "@layouts/main";
import { addProduct, removeProduct, CartSelectValue } from "@store/cart";
import { IProduct, ProductsQueryId } from "@typings/products";
import classNames from "@utils/classNames";
import textCapitalize from "@utils/textCapitalize";
import textTruncate from "@utils/textTruncate";
import {
	ArrowRightIcon,
	FunnelIcon,
	MagnifyingGlassIcon,
} from "@heroicons/react/24/outline";
import { ShoppingBagIcon } from "@heroicons/react/24/solid";
import { dehydrate, QueryClient } from "@tanstack/react-query";
import currency from "currency.js";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { signIn, useSession } from "next-auth/react";

const Products = ({ ...props }: any) => {
	const { status } = useSession();
	const { data, isLoading, isFetching, isError } = useProductsByCategory(
		props.id
	);

	const cart = useSelector(CartSelectValue);
	const dispatch = useDispatch();

	if (isError)
		return (
			<>
				<div className="pt-16 px-4 mx-auto">
					<ItemNotFound />
				</div>
			</>
		);
	if (isLoading || isFetching)
		return (
			<>
				<div className="pt-16 px-4 mx-auto flex justify-center">
					<Loader />
				</div>
			</>
		);

	const products = data.products.filter((product: IProduct) => {
		if (props.query === "") {
			return product;
		} else if (
			product.title.toLowerCase().includes(props.query.toLowerCase())
		) {
			return product;
		}
	});

	return products.length > 0 ? (
		<div className="grid grid-cols-3 gap-x-4 gap-y-8">
			{products.map((product: IProduct, index: number) => {
				const price = currency(product.price * 14818, {
					symbol: "Rp. ",
					decimal: ",",
					separator: ".",
					precision: 0,
				}).format();
				const title = textTruncate(29, product.title);

				return (
					<div key={index} className="hover:scale-[1.01] ease-in duration-100">
						<Link href={`/toko/produk/${product.id}`} passHref>
							<a>
								<div className="rounded-lg border aspect-square relative">
									<Image
										layout="fill"
										className="rounded-lg"
										alt={title}
										src={product.images[0]}
									/>
								</div>
								<h1 className="text-center text-xs mt-3 min-h-[32px]">
									{title}
								</h1>
								<p className="text-sm font-semibold mt-4">{price}</p>
							</a>
						</Link>

						<div className="flex justify-center mt-3">
							{cart.products.find((x: IProduct) => x.id === product.id) ? (
								<span className="flex justify-between items-center space-x-5">
									<button
										onClick={() =>
											status === "authenticated"
												? dispatch(
														removeProduct({
															product,
															quantity: 1,
														})
												  )
												: signIn()
										}
										className="w-6 border rounded-full border-primary-500 text-primary-500 font-semibold"
									>
										-
									</button>
									<span>
										{
											cart.products.find((x: IProduct) => x.id === product.id)
												.quantity
										}
									</span>
									<button
										onClick={() =>
											status === "authenticated"
												? dispatch(
														addProduct({
															product,
															quantity: 1,
														})
												  )
												: signIn()
										}
										className="w-6 border rounded-full bg-primary-500 text-white font-semibold"
									>
										+
									</button>
								</span>
							) : (
								<button
									onClick={() =>
										status === "authenticated"
											? dispatch(
													addProduct({
														product,
														quantity: 1,
													})
											  )
											: signIn()
									}
									className="py-2 px-4 text-xs font-semibold rounded-full border border-primary-500 text-primary-500 hover:scale-[1.05] ease-in duration-100"
								>
									Tambah
								</button>
							)}
						</div>
					</div>
				);
			})}
		</div>
	) : (
		<div className="mt-6">
			<ItemNotFound />
		</div>
	);
};

const TokoKategori = () => {
	const router = useRouter();
	const { kategoriId } = router.query;
	const pageTitle = kategoriId
		? textCapitalize(String(kategoriId))
		: "Kategori";

	const [query, setQuery] = useState("");

	const cart = useSelector(CartSelectValue);

	return (
		<>
			<Navigation title={pageTitle} />

			{/* Search Bar */}
			<div className="pt-16 px-4 mx-auto">
				<div className="flex items-center space-x-3 mt-2">
					<div className="flex justify-center items-center relative w-full">
						<MagnifyingGlassIcon className="absolute left-2 h-4 w-4 text-gray-700" />
						<input
							type="search"
							placeholder="Search"
							value={query}
							onChange={(e) => setQuery(e.target.value)}
							className="bg-gray-100 text-gray-600 border rounded-full pl-7 pr-3 py-2 text-sm w-full focus:!outline-0"
						/>
					</div>
					<button className="bg-primary-500 p-2 rounded-full">
						<FunnelIcon className="h-5 w-5 text-white" />
					</button>
				</div>
			</div>

			{/* Produk */}
			<div className="py-5 px-4 mx-auto relative">
				<div className={classNames(cart.totalQuantity > 0 ? "mb-20" : "mb-12")}>
					<Products id={kategoriId} query={query} />

					<div
						className={classNames(
							"fixed w-full left-0 ease-out duration-300",
							cart.totalQuantity > 0 ? "bottom-6" : "-bottom-96"
						)}
					>
						<div className="w-[375px] mx-auto">
							<Link href={`/keranjang`}>
								<a>
									<div className="flex items-center space-x-3 bg-primary-500 rounded-full mx-4 py-3 px-5 relative">
										<span>
											<ShoppingBagIcon className="w-5 h-5 text-white" />
										</span>
										<span className="text-white">
											<h2 className="text-[10px]">
												{cart.totalProducts} Produk Terpilih
											</h2>
											<h1 className="text-xs font-semibold">
												{currency(cart.total * 14818, {
													symbol: "Rp. ",
													decimal: ",",
													separator: ".",
													precision: 0,
												}).format()}
											</h1>
										</span>
										<span className="absolute right-5">
											<ArrowRightIcon className="w-5 h-5 text-white" />
										</span>
									</div>
								</a>
							</Link>
						</div>
					</div>
				</div>
			</div>
		</>
	);
};

export async function getServerProps(id: ProductsQueryId) {
	const queryClient = new QueryClient();

	await queryClient.prefetchQuery(["products", id], () =>
		fetchProductsByCategory(id)
	);

	return {
		props: {
			dehydratedState: dehydrate(queryClient),
		},
	};
}

TokoKategori.layout = MainLayout;

export default TokoKategori;
