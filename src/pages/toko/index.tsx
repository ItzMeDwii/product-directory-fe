import Navigation from "@components/Navigation";
import ItemNotFound from "@components/utils/ItemNotFound";
import Loader from "@components/utils/Loader";
import { fetchCategories, useCategories } from "@hooks/index";
import MainLayout from "@layouts/main";
import { ICategory } from "@typings/products";
import textCapitalize from "@utils/textCapitalize";
import { dehydrate, QueryClient } from "@tanstack/react-query";
import Image from "next/image";
import Link from "next/link";

const CategoriesDisplay = () => {
	const { data, isLoading, isFetching, isError } = useCategories();

	if (isError)
		return (
			<>
				<div className="pt-16 px-4 mx-auto">
					<ItemNotFound />
				</div>
			</>
		);
	if (isLoading || isFetching)
		return (
			<>
				<div className="pt-16 px-4 mx-auto flex justify-center">
					<Loader />
				</div>
			</>
		);

	return (
		<section className="mt-4 grid grid-cols-4 gap-x-4 gap-y-8">
			{data.map((x: ICategory, index: number) => (
				<Link
					href={`/toko/kategori/${encodeURIComponent(x)}`}
					key={index}
					className="col-span-1"
					passHref
				>
					<a className="hover:scale-105 ease-in duration-100">
						<Image
							layout="fixed"
							alt={x}
							src="/cat-1.png"
							width={73}
							height={73}
						/>
						<h1 className="text-center text-xs mt-3">{textCapitalize(x)}</h1>
					</a>
				</Link>
			))}
		</section>
	);
};

const Toko = () => {
	return (
		<>
			<Navigation title="Detail Toko" />

			{/* Detail Toko */}
			<div className="pt-16 pb-6 px-4 mx-auto">
				<div className="mt-2 grid grid-cols-4 gap-4">
					<div className="col-span-1 flex justify-center relative h-[74] w-[74]">
						<Image
							className="z-[102]"
							layout="fixed"
							alt="logo-toko-1"
							src="/images/toko/toko-1.png"
							width={74}
							height={74}
						/>
						<div className="z-[103] absolute bottom-1 rounded-full font-semibold px-2 py-1 text-xs text-black bg-primary-500">
							<div className="flex items-center space-x-1">
								<svg
									xmlns="http://www.w3.org/2000/svg"
									className="h-4 w-4 text-white"
									viewBox="0 0 20 20"
									fill="currentColor"
								>
									<path
										fillRule="evenodd"
										d="M3.172 5.172a4 4 0 015.656 0L10 6.343l1.172-1.171a4 4 0 115.656 5.656L10 17.657l-6.828-6.829a4 4 0 010-5.656z"
										clipRule="evenodd"
									/>
								</svg>
								<span>5.0</span>
							</div>
						</div>
					</div>
					<div className="col-span-3">
						<h1 className="text-sm font-semibold mb-1">Toko Semesta</h1>
						<p className="text-xs mb-2">
							Jl. Kasipah Raya No. 182, Jatingaleh, Kec. Candisari, Kota
							Semarang
						</p>
						<div className="flex space-x-2">
							<span className="rounded-full font-semibold px-2 py-1 text-[8px] text-white bg-primary-500">
								Express
							</span>
							<span className="rounded-full font-semibold px-2 py-1 text-[8px] text-primary-500 bg-green-200">
								09.00
							</span>
							<span className="rounded-full font-semibold px-2 py-1 text-[8px] text-primary-500 bg-green-200">
								16.00
							</span>
						</div>
					</div>
				</div>
			</div>

			<div className="bg-gray-100 h-2 w-full mt-5"></div>

			{/* Kategori */}
			<div className="py-5 px-4 mx-auto relative">
				<div className="mb-14">
					<h1 className="font-semibold">Kategori Produk</h1>

					<CategoriesDisplay />
				</div>

				<div className="fixed bottom-6 left-0 flex justify-center w-full">
					<button className="rounded-full shadow shadow-primary-500 font-semibold px-3 py-2 text-xs text-white bg-primary-500 hover:scale-105 ease-in duration-100">
						Jadikan Toko Favorit
					</button>
				</div>
			</div>
		</>
	);
};

export async function getServerProps() {
	const queryClient = new QueryClient();

	await queryClient.prefetchQuery(["categories"], () => fetchCategories());

	return {
		props: {
			dehydratedState: dehydrate(queryClient),
		},
	};
}

Toko.layout = MainLayout;

export default Toko;
