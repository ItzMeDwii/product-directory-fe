import Navigation from "@components/Navigation";
import ItemNotFound from "@components/utils/ItemNotFound";
import { createCart } from "@hooks/carts";
import MainLayout from "@layouts/main";
import {
	addProduct,
	removeProduct,
	resetCart,
	CartSelectValue,
} from "@store/cart";
import { addCart } from "@store/order";
import { IProduct } from "src/typings/products";
import classNames from "@utils/classNames";
import { RadioGroup } from "@headlessui/react";
import { PencilSquareIcon, XMarkIcon } from "@heroicons/react/24/outline";
import currency from "currency.js";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { useState } from "react";
import toast from "react-hot-toast";
import { useDispatch, useSelector } from "react-redux";

const MetodePengiriman = () => {
	const [shipping, setShipping] = useState("");

	const shippingMethods = [
		{
			id: 0,
			name: "Pickup / diambil",
			icon: "/images/cart/icon-pelayanan.png",
			value: "pickup",
		},
		{
			id: 1,
			name: "Ekspress / Kirim Cepat",
			icon: "/logo.png",
			value: "ekspress",
		},
		{
			id: 2,
			name: "09:00 - 11:00",
			icon: "/logo.png",
			value: "pagi",
		},
		{
			id: 3,
			name: "12:00 - 16:00",
			icon: "/logo.png",
			value: "siang",
		},
		{
			id: 4,
			name: "18:00 - 20:00",
			icon: "/logo.png",
			value: "malam",
		},
	];

	return (
		<section>
			<div className="mt-2">
				<h1 className="font-semibold">Pilih Metode Pengiriman</h1>
			</div>

			<hr className="mt-2 mb-1" />

			<RadioGroup value={shipping} onChange={setShipping}>
				{shippingMethods.map((ship, i) => (
					<RadioGroup.Option key={i} value={ship.value}>
						{({ checked }) => (
							<div className="flex items-center space-x-4 border rounded-xl bg-gray-100 py-4 px-4 mt-4 relative">
								<Image
									layout="fixed"
									alt={ship.name}
									src={ship.icon}
									width={27}
									height={30}
								/>
								<h1 className="font-semibold text-sm">{ship.name}</h1>
								<span
									className={classNames(
										"absolute flex justify-center items-center right-4 border-2 rounded-full w-6 h-6",
										checked ? "border-primary-500" : "border-gray-400"
									)}
								>
									<div
										className={classNames(
											"w-4 h-4 rounded-full",
											checked ? "bg-primary-500" : "bg-gray-400"
										)}
									/>
								</span>
							</div>
						)}
					</RadioGroup.Option>
				))}
			</RadioGroup>
		</section>
	);
};

const CartSection = ({ ...props }) => {
	const cart = useSelector(CartSelectValue);
	const dispatch = useDispatch();

	const Header = () => (
		<>
			<div className="flex justify-between items-center mt-2">
				<span>
					<h1 className="font-semibold">Pesanan</h1>
				</span>
				<span>
					<Link href={`/toko`} passHref>
						<a className="font-semibold text-xs text-primary-500">
							+ Tambah Pesanan
						</a>
					</Link>
				</span>
			</div>

			<hr className="mt-2 mb-1" />
		</>
	);

	if (cart.totalQuantity < 1)
		return (
			<section className={props.className}>
				<Header />
				<div className="mt-6">
					<ItemNotFound />
				</div>
			</section>
		);
	/* if (isLoading || isFetching) {
		return (
			<section className={props.className}>
				<Header />
				<div className="mt-6 flex justify-center">
					<Loader />
				</div>
			</section>
		);
	} */

	return (
		<section className={props.className}>
			<Header />
			<div className="mt-4">
				{cart.products.map((product: IProduct, index: number) => {
					return (
						<div
							className="flex space-x-4 w-full border-b border-dashed pb-6 mb-6"
							key={index}
						>
							<span className="flex items-center justify-center ">
								<button
									onClick={() =>
										dispatch(
											removeProduct({
												product,
												quantity: product.quantity,
											})
										)
									}
									className="flex justify-center items-center rounded-full w-5 h-5 bg-gray-300"
								>
									<XMarkIcon className="w-4 h-4 text-white" />
								</button>
							</span>
							<span className="w-full">
								<div>
									<h1 className="text-sm">{product.title}</h1>
									<div className="flex justify-between items-center w-full mt-2">
										<span>
											<h1 className="font-semibold">
												{currency(product.price * 14818, {
													symbol: "Rp. ",
													decimal: ",",
													separator: ".",
													precision: 0,
												}).format()}
											</h1>
										</span>

										<span className="flex justify-between items-center space-x-5">
											<button
												onClick={() =>
													dispatch(
														removeProduct({
															product,
															quantity: 1,
														})
													)
												}
												className="w-6 border rounded-full border-primary-500 text-primary-500 font-semibold"
											>
												-
											</button>
											<span>
												{
													cart.products.find(
														(x: IProduct) => x.id === product.id
													).quantity
												}
											</span>
											<button
												onClick={() =>
													dispatch(
														addProduct({
															product,
															quantity: 1,
														})
													)
												}
												className="w-6 border rounded-full bg-primary-500 text-white font-semibold"
											>
												+
											</button>
										</span>
									</div>
								</div>
							</span>
						</div>
					);
				})}
			</div>
			<div className="flex justify-between">
				<h1 className="text-sm font-semibold">Total Harga</h1>
				<h1 className="text-sm font-semibold">
					{currency(cart.total * 14818, {
						symbol: "Rp. ",
						decimal: ",",
						separator: ".",
						precision: 0,
					}).format()}
				</h1>
			</div>
		</section>
	);
};

const Keranjang = () => {
	const router = useRouter();
	const { keranjangId } = router.query;
	const cart = useSelector(CartSelectValue);
	const dispatch = useDispatch();

	const handleSubmitCart = async () => {
		if (cart.totalQuantity > 0) {
			const { isError } = await createCart(1, cart.products);

			if (!isError) {
				dispatch(addCart({ cart }));
				dispatch(resetCart());
				await router.push("/pesanan");
			}
		} else {
			toast.error("Tambahkan pesanan terlebih dahulu.");
		}
	};

	return (
		<>
			<Navigation title="Keranjang" />

			<div className="pt-16 px-4 mx-auto">
				<MetodePengiriman />

				<CartSection className="mt-8" id={keranjangId} />
			</div>

			<div className="bg-gray-100 h-2 w-full mt-5"></div>

			<section className="px-4 mx-auto">
				<div className="mt-4">
					<h1 className="font-semibold">Tambahkan Catatan</h1>

					<hr className="mt-4 mb-4" />

					<div className="flex justify-center items-center relative w-full">
						<PencilSquareIcon className="absolute left-4 h-4 w-4 text-gray-700" />
						<input
							type="text"
							placeholder="Masukkan catatan di sini"
							className="bg-gray-100 text-gray-600 rounded-xl pl-10 pr-4 py-4 text-sm w-full focus:!outline-0"
						/>
					</div>
				</div>
			</section>

			<section className="pb-6 mx-auto">
				<div className="flex justify-between mt-2 pt-4 px-4 border-t">
					<h1 className="text-sm font-semibold">Total Pembayaran</h1>
					<h1 className="text-sm font-semibold">
						{currency(cart.total * 14818, {
							symbol: "Rp. ",
							decimal: ",",
							separator: ".",
							precision: 0,
						}).format()}
					</h1>
				</div>

				<div className="flex justify-center w-full px-4 mt-4">
					<button
						onClick={() => handleSubmitCart()}
						className="w-full rounded-full text-sm font-semibold py-3 px-5 bg-primary-500 text-white"
					>
						Buat Pesanan
					</button>
				</div>
			</section>
		</>
	);
};

Keranjang.layout = MainLayout;

export default Keranjang;
