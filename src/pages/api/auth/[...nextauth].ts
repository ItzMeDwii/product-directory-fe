/* eslint-disable @typescript-eslint/require-await */
import axios from "axios";
import { NextApiRequest, NextApiResponse } from "next";
import NextAuth, { NextAuthOptions, User } from "next-auth";
import CredentialsProvider from "next-auth/providers/credentials";
import GoogleProvider from "next-auth/providers/google";

export const authOptions: NextAuthOptions = {
	secret: process.env.NEXTAUTH_SECRET,
	session: {
		strategy: "jwt",
		maxAge: 30 * 24 * 60 * 60, // 30 days
		updateAge: 24 * 60 * 60, // 24 hours
	},
	providers: [
		CredentialsProvider({
			name: "DummyJSON",
			credentials: {
				username: { label: "Username", type: "text" },
				password: { label: "Password", type: "password" },
			},
			async authorize(credentials) {
				try {
					const response = await axios.post(
						`https://dummyjson.com/auth/login`,
						{
							username: credentials.username,
							password: credentials.password,
						}
					);

					if (response) {
						return response.data;
					}
				} catch (error) {
					console.error(
						process.env.NODE_ENV === "development" ? error : error.message
					);
					return null;
				}
			},
		}),
		GoogleProvider({
			clientId: process.env.GOOGLE_ID,
			clientSecret: process.env.GOOGLE_SECRET,
		}),
	],
	theme: {
		colorScheme: "light",
	},
	callbacks: {
		async redirect({ url, baseUrl }) {
			// Allows relative callback URLs
			if (url.startsWith("/")) return `${baseUrl}${url}`;
			// Allows callback URLs on the same origin
			else if (new URL(url).origin === baseUrl) return url;
			return baseUrl;
		},
		async jwt({ token, account, user }) {
			if (user) {
				token.providerType = account.provider;
				token.user = user;
				token.accessToken = user.token;
			}
			return token;
		},

		async session({ session, token }) {
			session.user = token.user as User;
			if ((token.user as User).token) {
				session.user.token = "TOKEN";
			}
			session.provider = token.providerType;
			session.accessToken = token.accessToken;
			return session;
		},
	},
	pages: {
		signIn: "/masuk",
		error: "/masuk",
	},
	debug: process.env.NODE_ENV === "development",
};

export default async function Auth(req: NextApiRequest, res: NextApiResponse) {
	await NextAuth(req, res, authOptions);
}
