import "@styles/globals.css";
import {
	Hydrate,
	QueryClient,
	QueryClientProvider,
} from "@tanstack/react-query";
import { SessionProvider } from "next-auth/react";
import Head from "next/head";
import { useState } from "react";
import { Provider } from "react-redux";
import { AppLayoutProps } from "@typings/layouts";
import { wrapper } from "@store/index";

function MyApp({
	Component,
	session,
	dehydratedState,
	pageProps,
}: AppLayoutProps) {
	const Layout = Component.layout || (({ children }) => <>{children}</>);
	const { store, props } = wrapper.useWrappedStore(pageProps);
	const [queryClient] = useState(
		() =>
			new QueryClient({
				defaultOptions: {
					queries: {
						refetchOnWindowFocus: false,
					},
				},
			})
	);

	return (
		<>
			<Head>
				<title>Dwii WoiShop</title>

				<meta name="viewport" content="initial-scale=1" />
				<meta name="robots" content="index, nofollow" />

				<link rel="preconnect" href="https://fonts.googleapis.com" />
				<link
					rel="preconnect"
					href="https://fonts.gstatic.com"
					crossOrigin=""
				/>
			</Head>

			<QueryClientProvider client={queryClient}>
				<Hydrate state={dehydratedState}>
					<SessionProvider session={session}>
						<Provider store={store}>
							<Layout>
								<Component {...props} />
							</Layout>
						</Provider>
					</SessionProvider>
				</Hydrate>
			</QueryClientProvider>
		</>
	);
}

export default MyApp;
