import Input from "@components/Input";
import MainLayout from "@layouts/main";
import { KeyIcon, PhoneIcon } from "@heroicons/react/24/outline";
import { yupResolver } from "@hookform/resolvers/yup";
import { signIn, useSession } from "next-auth/react";
import Image from "next/image";
import Link from "next/link";
import { useRouter } from "next/router";
import { useState } from "react";
import { useForm } from "react-hook-form";
import toast from "react-hot-toast";
import * as yup from "yup";

type LoginFormFields = {
	username: string;
	password: string;
};

const schema = yup
	.object({
		username: yup.string().required("Nomor HP tidak boleh kosong!"),
		password: yup.string().required("Password tidak boleh kosong!"),
	})
	.required();

const AuthLogin = () => {
	const router = useRouter();
	const { status } = useSession();
	const { register, formState, handleSubmit, getFieldState, getValues } =
		useForm<LoginFormFields>({
			mode: "all",
			resolver: yupResolver(schema),
			defaultValues: {
				username: "kminchelle",
				password: "0lelplR",
			},
		});

	const [error, setError] = useState("");

	const onSubmit = handleSubmit(async (data) => {
		const { ok, error } = await signIn("credentials", {
			username: data.username,
			password: data.password,
			callbackUrl: router.query.callbackUrl as string,
		});

		if (ok) {
			toast.success("Login success");
		} else {
			error === "CredentialsSignin"
				? toast.error("Credentials do not match!") &&
				  setError("Credentials do not match!")
				: toast.error(error) && setError(error);
		}
	});

	return (
		<>
			{status === "authenticated" ? (
				setTimeout(() => router.push("/"), 100)
			) : (
				<div className="pt-16 pb-6 px-4 mx-auto">
					<div className="flex justify-center w-full">
						<Image
							layout="fixed"
							alt="login image"
							src="/image-1.png"
							height={231}
							width={226}
						/>
					</div>

					<div className="mt-8">
						<h1 className="text-center font-bold text-primary-500 text-2xl">
							Hai, fren!
						</h1>
						<h2 className="text-center">Selamat Datang di Aplikasi WoiShop</h2>
					</div>
					<form onSubmit={onSubmit}>
						<div className="mt-10">
							<Input<LoginFormFields>
								name="username"
								type="text"
								label="Nomor HP Anda"
								register={register}
								icon={PhoneIcon}
								fieldValue={getValues("username")}
								fieldState={getFieldState("username", formState)}
							/>
						</div>

						<div className="mt-4">
							<Input<LoginFormFields>
								name="password"
								type="password"
								label="Password Anda"
								register={register}
								icon={KeyIcon}
								fieldValue={getValues("password")}
								fieldState={getFieldState("password", formState)}
							/>
						</div>

						<div className="mt-5">
							<button
								type="submit"
								className="text-center text-sm font-semibold bg-primary-500 text-white rounded-full py-4 w-full"
							>
								Masuk
							</button>
						</div>

						{error.length > 0 && (
							<h1 className="mt-2 text-[10px] text-center text-red-500">
								{error}
							</h1>
						)}
					</form>

					<div className="mt-5 flex justify-center items-center space-x-2 w-full">
						<div className="h-[1px] w-full bg-gray-200" />
						<h1 className="w-full font-semibold text-gray-500 text-xs">
							Atau Login Melalui
						</h1>
						<div className="h-[1px] w-full bg-gray-200" />
					</div>

					<div className="mt-5">
						<button
							onClick={() =>
								signIn("google", {
									callbackUrl: router.query.callbackUrl as string,
								})
							}
							type="button"
							className="flex items-center justify-center space-x-1 border border-primary-500 rounded-full py-4 w-full"
						>
							<svg
								version="1.1"
								xmlns="http://www.w3.org/2000/svg"
								width="16px"
								height="16px"
								viewBox="0 0 48 48"
								className="abcRioButtonSvg"
							>
								<g>
									<path
										fill="#EA4335"
										d="M24 9.5c3.54 0 6.71 1.22 9.21 3.6l6.85-6.85C35.9 2.38 30.47 0 24 0 14.62 0 6.51 5.38 2.56 13.22l7.98 6.19C12.43 13.72 17.74 9.5 24 9.5z"
									></path>
									<path
										fill="#4285F4"
										d="M46.98 24.55c0-1.57-.15-3.09-.38-4.55H24v9.02h12.94c-.58 2.96-2.26 5.48-4.78 7.18l7.73 6c4.51-4.18 7.09-10.36 7.09-17.65z"
									></path>
									<path
										fill="#FBBC05"
										d="M10.53 28.59c-.48-1.45-.76-2.99-.76-4.59s.27-3.14.76-4.59l-7.98-6.19C.92 16.46 0 20.12 0 24c0 3.88.92 7.54 2.56 10.78l7.97-6.19z"
									></path>
									<path
										fill="#34A853"
										d="M24 48c6.48 0 11.93-2.13 15.89-5.81l-7.73-6c-2.15 1.45-4.92 2.3-8.16 2.3-6.26 0-11.57-4.22-13.47-9.91l-7.98 6.19C6.51 42.62 14.62 48 24 48z"
									></path>
									<path fill="none" d="M0 0h48v48H0z"></path>
								</g>
							</svg>
							<span className="text-sm font-semibold">Google</span>
						</button>
					</div>

					<div className="flex justify-center mt-4">
						<Link href="/daftar" passHref>
							<a className="text-center text-xs text-gray-500">
								Belum punya akun? Daftar Di Sini
							</a>
						</Link>
					</div>

					<div className="mt-6">
						<p className="text-xs text-gray-500">
							Dengan masuk atau mendaftar, kamu menyetujui Ketentuan Layanan dan
							Kebijakan Privasi
						</p>
					</div>
				</div>
			)}
		</>
	);
};

AuthLogin.layout = MainLayout;

export default AuthLogin;
