import MainLayout from "@layouts/main";
import Link from "next/link";
import BottomNavigation from "@components/BottomNavigation";
import {
	MagnifyingGlassIcon,
	ShoppingCartIcon,
} from "@heroicons/react/24/outline";
import { useSelector } from "react-redux";
import { CartSelectValue } from "@store/cart";

const Index = () => {
	const getCart = useSelector(CartSelectValue);

	return (
		<>
			<div className="pt-4 px-4 pb-14 mx-auto">
				<section>
					<div className="flex items-center space-x-3 mt-2">
						<div className="flex justify-center items-center relative w-full">
							<MagnifyingGlassIcon className="absolute left-3 h-4 w-4 text-gray-700" />
							<input
								type="search"
								placeholder="Cari"
								className="bg-gray-100 text-gray-600 rounded-full pl-9 pr-3 py-3 text-sm w-full focus:!outline-0 border"
							/>
						</div>
						<Link href="/keranjang" passHref>
							<button className="bg-gray-100 p-3 rounded-full relative border">
								<ShoppingCartIcon className="h-5 w-5 text-gray-500" />
								{getCart.products.length > 0 && (
									<span className="absolute flex items-center text-[8px] rounded-full bg-primary-500 text-white px-1 top-0 right-0">
										1
									</span>
								)}
							</button>
						</Link>
					</div>
				</section>

				<div className="mt-6 flex justify-center space-x-4 w-full">
					<Link href="/toko" passHref>
						<a className="text-primary-500 bg-green-200 py-2 px-3 rounded-full">
							Go to /toko
						</a>
					</Link>
				</div>
			</div>
			<BottomNavigation />
		</>
	);
};

Index.layout = MainLayout;

export default Index;
