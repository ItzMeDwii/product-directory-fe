export { default } from "next-auth/middleware";

export const config = {
	matcher: ["/keranjang", "/keranjang/:path*", "/pesanan", "/pesanan/:path*"],
};
