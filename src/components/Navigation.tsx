import {
	BellIcon,
	HomeIcon,
	QuestionMarkCircleIcon,
	QueueListIcon,
	UserIcon,
} from "@heroicons/react/24/outline";
import { useRouter } from "next/router";
import { SVGProps } from "react";

interface NavigationProps {
	title: string;
}

export interface NavRoutesProps {
	id: number;
	title: string;
	path: string[] | string;
	icon: (props: SVGProps<SVGSVGElement>) => JSX.Element;
}

export const NavHomeRoutes: NavRoutesProps[] = [
	{
		id: 0,
		title: "Beranda",
		path: "/",
		icon: HomeIcon,
	},
	{
		id: 1,
		title: "Bantuan",
		path: "/bantuan",
		icon: QuestionMarkCircleIcon,
	},
	{
		id: 2,
		title: "Transaksi",
		path: "/pesanan",
		icon: QueueListIcon,
	},
	{
		id: 3,
		title: "Notifikasi",
		path: "/notifikasi",
		icon: BellIcon,
	},
	{
		id: 4,
		title: "Profil",
		path: "/profil",
		icon: UserIcon,
	},
];

const Navigation = ({ title }: NavigationProps) => {
	const router = useRouter();

	return (
		<>
			<nav className="z-[1000] fixed top-0 w-[375px] mx-auto border-b-2 bg-white">
				<div className="mx-auto py-3 px-4">
					<div className="flex items-center space-x-4">
						{router.pathname !== "/" && (
							<nav>
								<button
									className="flex items-center"
									onClick={() => router.back()}
								>
									<svg
										width="18"
										height="17"
										viewBox="0 0 18 17"
										fill="none"
										xmlns="http://www.w3.org/2000/svg"
									>
										<path
											d="M16.2638 7.71768H4.31668L9.5362 2.49815C9.95334 2.08102 9.95334 1.39649 9.5362 0.979355C9.11907 0.56222 8.44524 0.56222 8.0281 0.979355L0.979599 8.02786C0.562464 8.44499 0.562464 9.11883 0.979599 9.53596L8.0281 16.5845C8.44524 17.0016 9.11907 17.0016 9.5362 16.5845C9.95334 16.1673 9.95334 15.4935 9.5362 15.0764L4.31668 9.85683H16.2638C16.8521 9.85683 17.3334 9.37553 17.3334 8.78726C17.3334 8.19899 16.8521 7.71768 16.2638 7.71768Z"
											fill="#03AC0E"
										/>
									</svg>
								</button>
							</nav>
						)}

						<h1 className="font-semibold">{title}</h1>
					</div>
				</div>
			</nav>
		</>
	);
};

export default Navigation;
