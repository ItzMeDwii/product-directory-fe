import Image from "next/image";

const ItemNotFound = () => {
    return (
        <div>
            <div className="flex justify-center items-center">
                <div className=" relative w-[253px] h-[238px]">
                    <Image layout="fill" src="/images/status/Ilustration-empty-state.png" alt="404" />
                </div>
            </div>
            <div className="mt-8">
                <h1 className="text-sm font-semibold text-center">
                    Item Tidak Ditemukan
                </h1>
                <p className="mt-3 text-sm text-center text-gray-600">
                    Maaf, sepertinya item yang Anda cari tidak ditemukan, coba cari yang lain yuk!
                </p>
            </div>
        </div>
    )
}

export default ItemNotFound;