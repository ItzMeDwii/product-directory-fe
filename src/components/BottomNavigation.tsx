import classNames from "@utils/classNames";
import Link from "next/link";
import { useRouter } from "next/router";
import { NavHomeRoutes, NavRoutesProps } from "./Navigation";

const BottomNavigation = () => {
	const router = useRouter();

	return (
		<>
			<nav className="z-[1000] fixed bottom-0 w-[375px] mx-auto border-t bg-white">
				<div className="grid grid-cols-5 space-x-4 px-4 w-full">
					{NavHomeRoutes.map((nav: NavRoutesProps) => {
						if (nav.title === "Transaksi") {
							return (
								<div className="mt-1 mb-2" key={nav.id}>
									<Link href={nav.path as string} passHref>
										<a
											className={classNames(
												"flex justify-center text-[10px] text-center relative",
												router.asPath === nav.path
													? "text-primary-500"
													: "text-gray-500"
											)}
										>
											<div className="w-12 h-12 absolute bottom-5 p-1 bg-white border-t-2 rounded-full">
												<nav.icon className="text-white bg-primary-500 p-2 rounded-full" />
											</div>
											<div className="mt-7 mb-0 font-semibold">{nav.title}</div>
										</a>
									</Link>
								</div>
							);
						}

						return (
							<div className="mt-1 mb-2" key={nav.id}>
								<Link href={nav.path as string} passHref>
									<a
										className={classNames(
											"text-[10px] text-center",
											router.asPath === nav.path
												? "text-primary-500"
												: "text-gray-500"
										)}
									>
										<div className="flex justify-center">
											<nav.icon className="w-6 h-6" />
										</div>
										<div className="mt-1 mb-0 font-semibold">{nav.title}</div>
									</a>
								</Link>
							</div>
						);
					})}
				</div>
			</nav>
		</>
	);
};

export default BottomNavigation;
