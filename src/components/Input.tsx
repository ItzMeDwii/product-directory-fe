import classNames from "@utils/classNames";
import { HTMLInputTypeAttribute, SVGProps, useState } from "react";
import {
	FieldError,
	Path,
	RegisterOptions,
	UseFormRegister,
} from "react-hook-form";

export type FormInputProps<TFormValues> = {
	register?: UseFormRegister<TFormValues>;
	rules?: RegisterOptions;
	fieldValue?: string;
	fieldState?: {
		invalid: boolean;
		isDirty: boolean;
		isTouched: boolean;
		error?: FieldError;
	};
	name: Path<TFormValues>;
	type?: HTMLInputTypeAttribute;
	label?: string;
	icon?: (props: SVGProps<SVGSVGElement>) => JSX.Element;
	className?: string;
};

const Input = <TFormValues extends Record<string, unknown>>({
	register,
	fieldValue,
	fieldState,
	name,
	type,
	label,
	icon: Icon,
	className,
}: FormInputProps<TFormValues>) => {
	const errorMessage = fieldState.error ? fieldState.error.message : "";
	const [isFocus, setIsFocus] = useState(false);

	return (
		<>
			<div className="bg-gray-100 border rounded-full flex justify-center items-center relative w-full">
				<Icon className="absolute left-5 h-4 w-4 text-gray-700 z-[1003]" />

				<label
					htmlFor={name}
					className={classNames(
						"absolute left-12 text-gray-600 w-full ease-in duration-100",
						isFocus || fieldValue.length > 0
							? "text-[10px] -translate-y-3 z-[1003]"
							: "text-sm z-[1001]"
					)}
				>
					{label}
				</label>

				<input
					name={name}
					type={type}
					{...register(name)}
					className={classNames(
						"bg-transparent text-black rounded-full pl-12 pr-6 pt-6 pb-2 text-sm w-full z-[1002] focus:!outline-0",
						fieldState.invalid && "!ring !ring-red-500",
						className
					)}
					onFocus={() => {
						setIsFocus(true);
					}}
					onBlur={() => {
						setIsFocus(false);
					}}
				/>
			</div>

			{errorMessage && (
				<div className="mt-2 text-[10px] text-center text-red-500">
					{errorMessage}
				</div>
			)}
		</>
	);
};

export default Input;
