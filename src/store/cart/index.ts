import { RootState } from "@store/index";
import { ICart } from "@typings/carts";
import { IProduct } from "@typings/products";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import toast from "react-hot-toast";
import { HYDRATE } from "next-redux-wrapper";

const initialState: ICart = {
	id: 0,
	products: [],
	total: 0,
	discountedTotal: 0,
	userId: 0,
	totalProducts: 0,
	totalQuantity: 0,
};

export const cartSlice = createSlice({
	name: "cart",
	initialState,
	reducers: {
		addProduct: (
			state,
			action: PayloadAction<{ product: IProduct; quantity: number }>
		) => {
			const { products }: ICart = state;
			const { product, quantity } = action.payload;
			const currentProduct = products.find(
				(x: IProduct) => x.id === product.id
			);

			state.total += product.price * quantity;

			state.totalProducts = currentProduct
				? state.totalProducts
				: state.totalProducts + 1;

			state.totalQuantity += quantity;

			if (currentProduct) {
				products[products.indexOf(currentProduct)] = {
					...currentProduct,
					quantity: (currentProduct.quantity as number) + quantity,
					total: (currentProduct.total as number) + product.price,
				};
			} else {
				products.push({
					...product,
					quantity,
					total: product.price,
				});
			}

			toast.success("Barang telah ditambahkan.", { duration: 1000 });
		},

		removeProduct: (
			state,
			action: PayloadAction<{ product: IProduct; quantity: number }>
		) => {
			const { products }: ICart = state;
			const { product, quantity } = action.payload;
			const currentProduct = products.find(
				(x: IProduct) => x.id === product.id
			);

			state.total -= product.price * quantity;

			state.totalProducts =
				currentProduct && currentProduct.quantity > quantity
					? state.totalProducts
					: state.totalProducts - 1;

			state.totalQuantity -= quantity;

			if (currentProduct && currentProduct.quantity > quantity) {
				products[products.indexOf(currentProduct)] = {
					...currentProduct,
					quantity: (currentProduct.quantity as number) - quantity,
					total: (currentProduct.total as number) - product.price,
				};
			} else {
				products.splice(products.indexOf(currentProduct), 1);
			}

			toast.success("Barang telah dihapus.", { duration: 1000 });
		},

		setCart: (state, action: PayloadAction<{ cart: ICart }>) => {
			return { ...action.payload.cart };
		},

		resetCart: () => initialState,
	},
	extraReducers: {
		[HYDRATE]: (state, action) => {
			return {
				...state,
				...action.payload,
			};
		},
	},
});

export const { addProduct, removeProduct, resetCart, setCart } =
	cartSlice.actions;

export const CartSelectValue = (state: RootState) => state.cart;

export default cartSlice.reducer;
