import { RootState } from "@store/index";
import { ICart } from "@typings/carts";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import toast from "react-hot-toast";
import { HYDRATE } from "next-redux-wrapper";

const initialState: ICart[] = [
	{
		id: 21,
		products: [
			{
				id: 1,
				title: "iPhone 9",
				price: 549,
				quantity: 4,
				total: 2196,
				discountPercentage: 12.96,
				discountedPrice: 1911,
			},
			{
				id: 2,
				title: "iPhone X",
				price: 899,
				quantity: 1,
				total: 899,
				discountPercentage: 17.94,
				discountedPrice: 738,
			},
			{
				id: 4,
				title: "OPPOF19",
				price: 280,
				quantity: 1,
				total: 280,
				discountPercentage: 17.91,
				discountedPrice: 230,
			},
			{
				id: 5,
				title: "Huawei P30",
				price: 499,
				quantity: 1,
				total: 499,
				discountPercentage: 10.58,
				discountedPrice: 446,
			},
		],
		total: 3874,
		discountedTotal: 3325,
		userId: 1,
		totalProducts: 4,
		totalQuantity: 7,
		createdAt: 1663733555294,
	},
	{
		id: 21,
		products: [
			{
				id: 37,
				title: "ank Tops for Womens/Girls",
				price: 50,
				quantity: 1,
				total: 50,
				discountPercentage: 12.05,
				discountedPrice: 44,
			},
			{
				id: 39,
				title: "Women Sweaters Wool",
				price: 600,
				quantity: 1,
				total: 600,
				discountPercentage: 17.2,
				discountedPrice: 497,
			},
			{
				id: 40,
				title: "women winter clothes",
				price: 57,
				quantity: 6,
				total: 342,
				discountPercentage: 13.39,
				discountedPrice: 296,
			},
		],
		total: 992,
		discountedTotal: 837,
		userId: 1,
		totalProducts: 3,
		totalQuantity: 8,
		createdAt: 1663734555294,
	},
];

export const orderSlice = createSlice({
	name: "order",
	initialState,
	reducers: {
		addCart: (state, action: PayloadAction<{ cart: ICart }>) => {
			const cart = action.payload.cart;
			const currentCart = state.find((x: ICart) => x.id === cart.id);
			if (currentCart) {
				toast.error("Sudah ada keranjang dengan ID yang sama.");
			} else {
				state.push({
					...cart,
					id: cart.id === 0 ? Date.now() : cart.id,
					createdAt: Date.now(),
				});
			}
		},

		removeCart: (state, action: PayloadAction<{ cart: ICart }>) => {
			const cart = action.payload.cart;
			const currentCart = state.find((x: ICart) => x.id === cart.id);
			if (currentCart) {
				state.splice(state.indexOf(currentCart), 1);
			} else {
				toast.error("Keranjang tidak ditemukan.");
			}
		},

		resetOrder: () => initialState,
	},

	extraReducers: {
		[HYDRATE]: (state, action) => {
			return {
				...state,
				...action.payload,
			};
		},
	},
});

export const { addCart, removeCart, resetOrder } = orderSlice.actions;

export const OrderSelectValue = (state: RootState) => state.order;

export default orderSlice.reducer;
