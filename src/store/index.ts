import cartReducer from "@store/cart";
import orderReducer from "@store/order";
import {
	Action,
	AnyAction,
	combineReducers,
	configureStore,
	ThunkAction,
} from "@reduxjs/toolkit";
import { createWrapper, HYDRATE } from "next-redux-wrapper";

const combinedReducer = combineReducers({
	cart: cartReducer,
	order: orderReducer,
});

const reducer = (
	state: ReturnType<typeof combinedReducer>,
	action: AnyAction
) => {
	if (action.type === HYDRATE) {
		const nextState = {
			...state, // use previous state
			...action.payload, // apply delta from hydration
		};
		return nextState;
	}
	return combinedReducer(state, action);
};

export const makeStore = () =>
	configureStore({
		reducer,
	});

type Store = ReturnType<typeof makeStore>;

export type AppDispatch = Store["dispatch"];
export type RootState = ReturnType<Store["getState"]>;
// eslint-disable-next-line @typescript-eslint/no-invalid-void-type
export type AppThunk<ReturnType = void> = ThunkAction<
	ReturnType,
	RootState,
	unknown,
	Action<string>
>;

export const wrapper = createWrapper(makeStore, {
	debug: process.env.NODE_ENV === "development",
});
