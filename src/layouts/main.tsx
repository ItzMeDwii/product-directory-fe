import { ToastBar, Toaster } from "react-hot-toast";
import { LayoutProps } from "@typings/layouts";

const MainLayout: LayoutProps = ({ children }) => (
	<>
		<div className="font-rubik text-black min-h-screen overflow-x-hidden z-[-1] sm:bg-neutral-100">
			<div className="bg-white max-w-[375px] mx-auto relative">
				<Toaster>{(t) => <ToastBar toast={t} />}</Toaster>
				<div className="min-h-screen">{children}</div>
			</div>
		</div>
	</>
);

export default MainLayout;
