import { ICategories } from "../../typings/products";
import { useQuery } from "@tanstack/react-query";
import axios from "axios";

const fetchCategories = async () => {
	try {
		const response = await axios.get(
			"https://dummyjson.com/products/categories"
		);
		const data: ICategories = response.data;
		return data;
	} catch (error) {
		console.error(
			process.env.NODE_ENV === "development" ? error : error.message
		);
	}
};

const useCategories = () => {
	return useQuery(["categories"], () => fetchCategories());
};

export { useCategories, fetchCategories };
