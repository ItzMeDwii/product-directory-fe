import { IProduct, ProductsQueryId } from "../../typings/products";
import { useQuery } from "@tanstack/react-query";
import axios from "axios";

const fetchProduct = async (id: ProductsQueryId) => {
	try {
		const response = await axios.get(
			`https://dummyjson.com/products/${encodeURIComponent(String(id))}`
		);

		const data: IProduct = response.data;

		return data;
	} catch (error) {
		console.error(
			process.env.NODE_ENV === "development" ? error : error.message
		);
	}
};

const useProduct = (id: ProductsQueryId) => {
	return useQuery(["product", id], () => fetchProduct(id));
};

export { useProduct, fetchProduct };
