import { IProducts, ProductsQueryId } from "../../typings/products";
import { useQuery } from "@tanstack/react-query";
import axios from "axios";

const fetchProductsByCategory = async (id: ProductsQueryId) => {
	try {
		const response = await axios.get(
			`https://dummyjson.com/products/category/${encodeURIComponent(
				String(id)
			)}`
		);

		const data: IProducts = response.data;

		return data;
	} catch (error) {
		console.error(
			process.env.NODE_ENV === "development" ? error : error.message
		);
	}
};

const useProductsByCategory = (id: ProductsQueryId) => {
	return useQuery(["products", id], () => fetchProductsByCategory(id));
};

export { useProductsByCategory, fetchProductsByCategory };
