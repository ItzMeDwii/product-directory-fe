import { CartsQueryId, ICart } from "../../typings/carts";
import { IProduct } from "../../typings/products";
import { useQuery } from "@tanstack/react-query";
import axios from "axios";
import toast from "react-hot-toast";

const fetchCart = async (id: CartsQueryId) => {
	try {
		const response = await axios.get(
			`https://dummyjson.com/carts/${encodeURIComponent(String(id))}`
		);

		const data: ICart = response.data;

		return data;
	} catch (error) {
		console.error(
			process.env.NODE_ENV === "development" ? error : error.message
		);
	}
};

const useCart = (id: CartsQueryId) => {
	return useQuery(["carts", id], () => fetchCart(id));
};

const createCart = async (userId: number, products: IProduct[]) => {
	try {
		const response = await axios.post(`https://dummyjson.com/carts/add`, {
			userId,
			products,
		});

		toast.success("Pesanan telah dibuat.");

		const data: ICart = response.data;

		return { data, isError: false };
	} catch (error) {
		console.error(
			process.env.NODE_ENV === "development" ? error : error.message
		);
		return { data: null, isError: true };
	}
};

export { useCart, fetchCart, createCart };
