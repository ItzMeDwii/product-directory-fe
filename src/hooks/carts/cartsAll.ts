import { ICarts } from "../../typings/carts";
import { useQuery } from "@tanstack/react-query";
import axios from "axios";

const fetchCarts = async (limit: number, skip: number, total: number) => {
	try {
		const response = await axios.get(
			`https://dummyjson.com/carts?limit=${limit}&skip=${skip}&total=${total}`
		);

		const data: ICarts = response.data;

		return data;
	} catch (error) {
		console.error(
			process.env.NODE_ENV === "development" ? error : error.message
		);
	}
};

const useCarts = (limit: number, skip: number, total: number) => {
	return useQuery(["carts", limit, skip, total], () =>
		fetchCarts(limit, skip, total)
	);
};

export { useCarts, fetchCarts };
